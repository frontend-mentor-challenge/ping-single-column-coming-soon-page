app()

function app() {


    const d = document
    const form = d.querySelector('.contact-form')
    const emailInput = d.querySelector('input[name="email"]')
    const errorMessageInput = d.querySelector('.form-field__error-message')
    const container = d.querySelector('.form-field')
    const validEmail = /^\w+([.-_+]?\w+)*@\w+([.-]?\w+)*(\.\w{2,10})+$/

    form.addEventListener('submit', validarEmail)

    function validarEmail(e) {
        e.preventDefault()

        if (emailInput.classList.contains('contact-form__input--error')) {
            setTimeout(() => {
                document.getElementsByClassName('form-field')[0].style.marginBottom = '0'
                emailInput.classList.remove('contact-form__input--error')
                errorMessageInput.innerHTML = ''
            }, 3000)
        }

        if (emailInput.value === '') {
            emailInput.classList.add('contact-form__input--error')
            errorMessageInput.innerHTML = 'Whoops! It looks like you forgot to add your email'
            document.getElementsByClassName('form-field')[0].style.marginBottom = '2rem'
            return
        }

        if (!validEmail.test(emailInput.value)) {
            emailInput.classList.add('contact-form__input--error')
            errorMessageInput.innerHTML = 'Please provide a valid email address'
            document.getElementsByClassName('form-field')[0].style.marginBottom = '2rem'
            return
        }
    }


}