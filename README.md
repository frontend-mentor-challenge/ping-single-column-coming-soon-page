# Frontend Mentor - Ping coming soon page solution

This is a solution to the [Ping coming soon page challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/ping-single-column-coming-soon-page-5cadd051fec04111f7b848da). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the site depending on their device's screen size
- See hover states for all interactive elements on the page
- Submit their email address using an `input` field
- Receive an error message when the `form` is submitted if:
	- The `input` field is empty. The message for this error should say *"Whoops! It looks like you forgot to add your email"*
	- The email address is not formatted correctly (i.e. a correct email address should have this structure: `name@host.tld`). The message for this error should say *"Please provide a valid email address"*

### Screenshot

![Mobile](./screenshots/mobile.jpg)
![Desktop](./screenshots/desktop.jpg)

### Links

- Solution URL: [Gitlab Repository](https://gitlab.com/frontend-mentor-challenge/ping-single-column-coming-soon-page)
- Live Site URL: [Live](https://ping-single-column-coming-soon-page.onrender.com)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- Mobile-first workflow
- [Normalize](https://necolas.github.io/normalize.css/) - CSS Reset
- [Google Fonts](https://fonts.google.com/)
- [SASS](https://sass-lang.com/) - SASS

### What I learned

I learn how to use javascript to validate a input.

### Continued development

### Useful resources

- [Figma](https://figma.com) - For design

**Note: Delete this note and replace the list above with resources that helped you during the challenge. These could come in handy for anyone viewing your solution or for yourself when you look back on this project in the future.**

## Author

- Website - [Issac Leyva](In construction)
- Frontend Mentor - [@issleyva](https://www.frontendmentor.io/profile/issleyva)

## Acknowledgments